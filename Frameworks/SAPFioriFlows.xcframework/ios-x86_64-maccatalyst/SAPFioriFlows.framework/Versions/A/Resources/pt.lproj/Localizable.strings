/* XTIT: Info Screen text when applying the NUIStyleSheet. */
"applyNUIStyleSheetApplyStepInfoScreenText" = "Aplicando NUIStyleSheet.";

/* XTIT: Info Screen text when applying the SAP Mobile Services LogSettings. */
"applySAPcpmsLogSettingsStepInfoScreenText" = "Aplicando configurações de log de SAP Mobile Services.";

/* XFLD: Text information if the provided credentials were invalid. */
"authenticationFailedMessage" = "Falha na autenticação.";

/* XTIT: Info Screen text before presenting the Basic Authentication view. */
"basicAuthenticationStepInfoScreenText" = "Validando credenciais.";

/* XTIT: Title of Alert window when asking for confirmation about cancelling the onboarding flow. */
"cancelAlertControllerTitleText" = "Tem certeza de que deseja cancelar o processo de integração?";

/* XTIT: Title of Alert window when asking for confirmation about cancelling the restore flow. */
"cancelRestoreAlertControllerTitleText" = "Tem certeza de que deseja cancelar o processo de restauração?";

/* Title of Alert window when asking for confirmation about triggering the reset passcode flow. */
"resetPasscodeAlertControllerTitleText" = "Para reinicializar o código de segurança, o perfil do usuário atual será excluído do dispositivo. Inicie a sessão novamente para criar um outro código de segurança";

/* XTIT: Title of Alert window when asking for confirmation about cancelling the passcode change flow. */
"cancelPasscodeChangeControllerTitleText" = "Tem certeza de que deseja cancelar o processo de alteração do código de segurança?";

/* XTIT: Title of Alert window when asking for confirmation about reset the application. */
"cancelPasscodeControllerTitleText" = "Tem certeza que deseja reinicializar os aplicativos?";

/* XTIT: Info Screen text when running the Composite Steps. */
"compositeStepInfoScreenText" = "Etapas compostas.";

/* XTIT: Info Screen text when downloading the SAP Mobile Services ClientResources. */
"downloadSAPcpmsClientResourcesStepInfoScreenText" = "Baixando recursos de cliente de SAP Mobile Services.";

/* XTIT: Info Screen text when downloading the SAP Mobile Services Destinations. */
"downloadSAPcpmsDestinationsStepInfoScreenText" = "Baixando destinos de SAP Mobile Services.";

/* XTIT: Info Screen text when downloading the SAP Mobile Services Settings. */
"downloadSAPcpmsSettingsStepInfoScreenText" = "Baixando configurações de SAP Mobile Services.";

/* XBUT: Text of button on FUIFeedbackScreen when the Face ID got disabled by passcode policy. */
"fuiFeedbackDisableFaceIDButtonText" = "Continuar";

/* XBUT: Text of button on FUIFeedbackScreen when the Touch ID is disabled by the new passcode policy */
"fuiFeedbackDisableTouchIDButtonText" = "Continuar";

/* XBUT: Text of button on FUIFeedbackScreen when the Face ID is enabled by the new passcode policy */
"fuiFeedbackEnableFaceIDButtonText" = "Ativar";

/* XBUT: Text of button on FUIFeedbackScreen when the Touch ID is enabled by the new passcode policy */
"fuiFeedbackEnableTouchIDButtonText" = "Ativar";

/* XTIT: FUIFeedbackScreen headline description text when faceID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableFaceIDDetailDescription" = "Utilize o código de segurança para efetuar logon na próxima vez.";

/* XTIT: FUIFeedbackScreen headline text when faceID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableFaceIDHeadlineDescription" = "O Face ID não estará mais disponível";

/* XTIT: FUIFeedbackScreen title text when faceID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableFaceIDTitleText" = "Alterações de política";

/* XTIT: FUIFeedbackScreen headline description text when touchID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableTouchIDDetailDescription" = "Utilize o código de segurança para efetuar logon na próxima vez.";

/* XTIT: FUIFeedbackScreen headline text when touchID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableTouchIDHeadlineDescription" = "O Touch ID não estará mais disponível";

/* XTIT: FUIFeedbackScreen title text when touchID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableTouchIDTitleText" = "Alterações de política";

/* XTIT: FUIFeedbackScreen detail description text when faceID is disabled by the new passcode policy. */
"fuiFeedbackScreenEnableFaceIDDetailDescription" = "Você pode ativar ou desativar esse recurso a qualquer momento em Configurações.";

/* XTIT: FUIFeedbackScreen headline text when passcode policy changed and face ID is enabled. */
"fuiFeedbackScreenEnableFaceIDHeadlineDescription" = "O Face ID agora está disponível! Ao ativar o Face ID, você terá acesso mais rápido às suas informações.";

/* XTIT: FUIFeedbackScreen title text when Face ID is enabled in the new passcode policy. */
"fuiFeedbackScreenEnableFaceIDTitleText" = "Face ID";

/* XTIT: FUIFeedbackScreen title text when passcode policy change is necessary. */
"fuiFeedbackScreenEnableTouchIDDetailDescription" = "Você pode ativar ou desativar esse recurso a qualquer momento em Configurações.";

/* XTIT: FUIFeedbackScreen headline text when passcode policy changed and touch ID is enabled. */
"fuiFeedbackScreenEnableTouchIDHeadlineDescription" = "O Touch ID agora está disponível! Ao ativar o Touch ID, você terá acesso mais rápido às suas informações.";

/* XTIT: FUIFeedbackScreen title text when touchID is enabled by the new passcode policy. */
"fuiFeedbackScreenEnableTouchIDTitleText" = "Touch ID";

/* XBUT: Text of button on FUIFeedbackScreen when passcode change is necessary. */
"fuiFeedbackScreenPasscodePolicyChangedActionButtonText" = "Continuar";

/* XMSG: FUIFeedbackScreen description when passcode policy change is necessary. */
"fuiFeedbackScreenPasscodePolicyChangedDescriptionLabel" = "Crie outro código de segurança.";

/* XTIT: FUIFeedbackScreen title text when passcode policy change is necessary. */
"fuiFeedbackScreenPasscodePolicyChangedHeadlineText" = "A política de código de segurança foi alterada";

/* XTIT: FUIFeedbackScreen title text when passcode policy change is necessary. */
"fuiFeedbackScreenPasscodePolicyChangedTitleText" = "Alterações de política";

/* XTIT: FUIFeedbackScreen headline description text when passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeAndFaceIDDetailDescription" = "A partir de agora, não será mais preciso utilizar código de segurança nem Face ID para efetuar logon. \n\n Você entrará diretamente no aplicativo.";

/* XTIT: FUIFeedbackScreen headline description text when passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeAndTouchIDDetailDescription" = "A partir de agora, não será mais preciso utilizar código de segurança nem Touch ID para efetuar logon. \n\n Você entrará diretamente no aplicativo.";

/* XBUT: Text of button on FUIFeedbackScreen when the passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeButtonText" = "OK";

/* XTIT: FUIFeedbackScreen headline description text when passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeDetailDescription" = "A partir de agora, não será mais preciso utilizar o código de segurança para efetuar logon. \n\n Você entrará diretamente no aplicativo.";

/* XTIT: FUIFeedbackScreen headline text when passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeHeadlineDescription" = "Política de logon alterada";

/* XTIT: FUIFeedbackScreen title text when passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeTitleText" = "Aviso";

/* XBUT: Text of button on FUIFeedbackScreen after migration when passcode creation is mandatory. */
"migrationOnlyBiometricIDWasSetPreviouslyButtonText" = "Continuar";

/* XTIT: Description of FUIFeedbackScreen after migration when previously only biometricID was set. */
"migrationOnlyBiometricIDWasSetPreviouslyDescriptionText" = "A partir desta versão, é obrigatório o uso de código de segurança. Crie um código de segurança para o aplicativo.";

/* XTIT: Title of FUIFeedbackScreen after migration when previously only biometricID was set. */
"migrationOnlyBiometricIDWasSetPreviouslyHeadlineText" = "Código de segurança obrigatório";

/* XBUT: Text of No button. */
"noButtonText" = "Não";

/* XTIT: Info Screen text before presenting the OAuth2 Authentication view. */
"oauth2AuthenticationStepInfoScreenText" = "Validando credenciais OAuth 2.0.";

/* XTIT: Text of PasscodeInputController when user wants to enable face ID. */
"passcodeInputControllerEnableFaceIDText" = "Para ativar o Face ID, insira seu código de segurança:";

/* XTIT: Text of PasscodeInputController when user wants to enable touch ID. */
"passcodeInputControllerEnableTouchIDText" = "Para ativar o Touch ID, insira seu código de segurança:";

/* XTIT: Title of Alert window when asking for confirmation about cancelling the onboarding flow. */
"rejectAlertControllerTitleText" = "Você não conseguirá continuar com a integração se não nos der seu consentimento.";

/* XBUT: Text of No button of reject. */
"rejectNoButtonText" = "Consentir";

/* XBUT: Text of Yes button of reject. */
"rejectYesButtonText" = "Sair do processo";

/* XTIT: Info Screen text before presenting the SAML Authentication view. */
"samlAuthenticationStepInfoScreenText" = "Validando credenciais SAML.";

/* XTIT: Info Screen text before presenting the SAP Mobile Services UserIdentity Discovery Authentication view. */
"sapcpmsUserIdentityDiscoveryAuthenticationStepInfoScreenText" = "Validando credenciais de descoberta de identidade do usuário de SAP Mobile Services.";

/* XTIT: Info Screen text before presenting the SLS Authentication view. */
"slsAuthenticationStepInfoScreenText" = "Validando credenciais SLS.";

/* XMSG: Message for the user to fill all the fields for authentication. */
"slsFillAllOfTheFieldsErrorMessage" = "Preencha todos os campos";

/* XBUT: Text of Yes button. */
"yesButtonText" = "Sim";

/* XBUT: Text of Continue button. */
"continueButtonText" = "Continuar";

/* XBUT: Text of Cancel button. */
"cancelButtonText" = "Cancelar";

/* XTIT: Title of Usage Collection Consent onboarding flow. */
"UsageCollectionConsentTitle" = "Coleta de dados de utilização";

/* XTIT: Body of Usage Collection Consent onboarding flow. */
"UsageCollectionConsentBody" = "O administrador do aplicativo solicita que você permita a coleta de dados sobre como você usa o aplicativo para obter insights a fim de melhorá-lo. A coleta de dados inclui informações sobre como você usa o aplicativo, como o tempo que passa usando-o ou como você navega nele.";

/* XTIT: Usage Collection Consent opt-out alert message. */
"usageOptOutAlertMessage" = "Deseja cancelar a participação?";

/* XTIT: Usage Collection Consent opt-in alert message. */
"usageOptInAlertMessage" = "Deseja participar?";

/* XTIT: Title of Crash Report Collection Consent onboarding flow. */
"CrashReportCollectionConsentTitle" = "Coleta de relatório de falhas";

/* XTIT: Body of Crash Report Collection Consent onboarding flow. */
"CrashReportCollectionConsentBody" = "O administrador do aplicativo solicita que você permita a coleta de dados para diagnosticar e analisar falha no aplicativo a fim de melhorar a estabilidade. A coleta de dados está limitada a relatórios de falha e informações do dispositivo.";

