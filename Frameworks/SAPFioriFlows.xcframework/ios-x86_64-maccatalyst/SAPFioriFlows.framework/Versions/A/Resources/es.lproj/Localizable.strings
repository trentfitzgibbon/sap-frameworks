/* XTIT: Info Screen text when applying the NUIStyleSheet. */
"applyNUIStyleSheetApplyStepInfoScreenText" = "Aplicación de NUIStyleSheet.";

/* XTIT: Info Screen text when applying the SAP Mobile Services LogSettings. */
"applySAPcpmsLogSettingsStepInfoScreenText" = "Opciones de registro de aplicación SAP Mobile Services.";

/* XFLD: Text information if the provided credentials were invalid. */
"authenticationFailedMessage" = "La autenticación ha fallado.";

/* XTIT: Info Screen text before presenting the Basic Authentication view. */
"basicAuthenticationStepInfoScreenText" = "Se están validando las credenciales.";

/* XTIT: Title of Alert window when asking for confirmation about cancelling the onboarding flow. */
"cancelAlertControllerTitleText" = "¿Está seguro de que desea cancelar el proceso de incorporación?";

/* XTIT: Title of Alert window when asking for confirmation about cancelling the restore flow. */
"cancelRestoreAlertControllerTitleText" = "¿Está seguro de que desea cancelar el proceso de restauración?";

/* Title of Alert window when asking for confirmation about triggering the reset passcode flow. */
"resetPasscodeAlertControllerTitleText" = "Para resetear la contraseña se eliminará del dispositivo el perfil de usuario actual. Vuelva a iniciar sesión para crear un nuevo código de acceso";

/* XTIT: Title of Alert window when asking for confirmation about cancelling the passcode change flow. */
"cancelPasscodeChangeControllerTitleText" = "¿Seguro que desea cancelar el proceso de modificación de código de acceso?";

/* XTIT: Title of Alert window when asking for confirmation about reset the application. */
"cancelPasscodeControllerTitleText" = "¿Está seguro que desea reiniciar la aplicación?";

/* XTIT: Info Screen text when running the Composite Steps. */
"compositeStepInfoScreenText" = "Pasos de compuesto.";

/* XTIT: Info Screen text when downloading the SAP Mobile Services ClientResources. */
"downloadSAPcpmsClientResourcesStepInfoScreenText" = "Descargando ClientResources de SAP Mobile Services.";

/* XTIT: Info Screen text when downloading the SAP Mobile Services Destinations. */
"downloadSAPcpmsDestinationsStepInfoScreenText" = "Descargando destinos de SAP Mobile Services.";

/* XTIT: Info Screen text when downloading the SAP Mobile Services Settings. */
"downloadSAPcpmsSettingsStepInfoScreenText" = "Descargando opciones de SAP Mobile Services.";

/* XBUT: Text of button on FUIFeedbackScreen when the Face ID got disabled by passcode policy. */
"fuiFeedbackDisableFaceIDButtonText" = "Continuar";

/* XBUT: Text of button on FUIFeedbackScreen when the Touch ID is disabled by the new passcode policy */
"fuiFeedbackDisableTouchIDButtonText" = "Continuar";

/* XBUT: Text of button on FUIFeedbackScreen when the Face ID is enabled by the new passcode policy */
"fuiFeedbackEnableFaceIDButtonText" = "Activar";

/* XBUT: Text of button on FUIFeedbackScreen when the Touch ID is enabled by the new passcode policy */
"fuiFeedbackEnableTouchIDButtonText" = "Activar";

/* XTIT: FUIFeedbackScreen headline description text when faceID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableFaceIDDetailDescription" = "Utilice el código de acceso para iniciar sesión la próxima vez.";

/* XTIT: FUIFeedbackScreen headline text when faceID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableFaceIDHeadlineDescription" = "Face ID ya no estará disponible";

/* XTIT: FUIFeedbackScreen title text when faceID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableFaceIDTitleText" = "Modificaciones en política";

/* XTIT: FUIFeedbackScreen headline description text when touchID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableTouchIDDetailDescription" = "Utilice el código de acceso para iniciar sesión la próxima vez.";

/* XTIT: FUIFeedbackScreen headline text when touchID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableTouchIDHeadlineDescription" = "Touch ID ya no estará disponible";

/* XTIT: FUIFeedbackScreen title text when touchID is disabled by the new passcode policy. */
"fuiFeedbackScreenDisableTouchIDTitleText" = "Modificaciones en política";

/* XTIT: FUIFeedbackScreen detail description text when faceID is disabled by the new passcode policy. */
"fuiFeedbackScreenEnableFaceIDDetailDescription" = "Puede activar o desactivar esta función en cualquier momento desde Configuración.";

/* XTIT: FUIFeedbackScreen headline text when passcode policy changed and face ID is enabled. */
"fuiFeedbackScreenEnableFaceIDHeadlineDescription" = "Face ID ya está disponible. Al activar Face ID podrá acceder más rápidamente a la información.";

/* XTIT: FUIFeedbackScreen title text when Face ID is enabled in the new passcode policy. */
"fuiFeedbackScreenEnableFaceIDTitleText" = "Face ID";

/* XTIT: FUIFeedbackScreen title text when passcode policy change is necessary. */
"fuiFeedbackScreenEnableTouchIDDetailDescription" = "Puede activar o desactivar esta función en cualquier momento desde Configuración.";

/* XTIT: FUIFeedbackScreen headline text when passcode policy changed and touch ID is enabled. */
"fuiFeedbackScreenEnableTouchIDHeadlineDescription" = "Touch ID ya está disponible. Al activar Touch ID podrá acceder más rápidamente a la información.";

/* XTIT: FUIFeedbackScreen title text when touchID is enabled by the new passcode policy. */
"fuiFeedbackScreenEnableTouchIDTitleText" = "Touch ID";

/* XBUT: Text of button on FUIFeedbackScreen when passcode change is necessary. */
"fuiFeedbackScreenPasscodePolicyChangedActionButtonText" = "Continuar";

/* XMSG: FUIFeedbackScreen description when passcode policy change is necessary. */
"fuiFeedbackScreenPasscodePolicyChangedDescriptionLabel" = "Cree un código de acceso nuevo.";

/* XTIT: FUIFeedbackScreen title text when passcode policy change is necessary. */
"fuiFeedbackScreenPasscodePolicyChangedHeadlineText" = "Ha cambiado la política de códigos de acceso";

/* XTIT: FUIFeedbackScreen title text when passcode policy change is necessary. */
"fuiFeedbackScreenPasscodePolicyChangedTitleText" = "Modificaciones en política";

/* XTIT: FUIFeedbackScreen headline description text when passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeAndFaceIDDetailDescription" = "A partir de ahora, ya no serán necesarios el código de acceso y Face ID para iniciar sesión. \n\n Entrará en la aplicación directamente.";

/* XTIT: FUIFeedbackScreen headline description text when passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeAndTouchIDDetailDescription" = "A partir de ahora, ya no serán necesarios el código de acceso y Touch ID para iniciar sesión. \n\n Entrará en la aplicación directamente.";

/* XBUT: Text of button on FUIFeedbackScreen when the passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeButtonText" = "OK";

/* XTIT: FUIFeedbackScreen headline description text when passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeDetailDescription" = "A partir de ahora, ya no será necesario el código de acceso para iniciar sesión. \n\n Entrará en la aplicación directamente.";

/* XTIT: FUIFeedbackScreen headline text when passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeHeadlineDescription" = "Se ha modificado la política de inicio de sesión";

/* XTIT: FUIFeedbackScreen title text when passcode policy is turned off on the server. */
"fuiFeedbackScreenTurnOffPasscodeTitleText" = "Aviso";

/* XBUT: Text of button on FUIFeedbackScreen after migration when passcode creation is mandatory. */
"migrationOnlyBiometricIDWasSetPreviouslyButtonText" = "Continuar";

/* XTIT: Description of FUIFeedbackScreen after migration when previously only biometricID was set. */
"migrationOnlyBiometricIDWasSetPreviouslyDescriptionText" = "A partir de esta versión, el código de acceso es obligatorio. Cree un código de acceso para la aplicación.";

/* XTIT: Title of FUIFeedbackScreen after migration when previously only biometricID was set. */
"migrationOnlyBiometricIDWasSetPreviouslyHeadlineText" = "Código de acceso obligatorio";

/* XBUT: Text of No button. */
"noButtonText" = "No";

/* XTIT: Info Screen text before presenting the OAuth2 Authentication view. */
"oauth2AuthenticationStepInfoScreenText" = "Se están validando las credenciales OAuth 2.0.";

/* XTIT: Text of PasscodeInputController when user wants to enable face ID. */
"passcodeInputControllerEnableFaceIDText" = "Para activar Face ID, introduzca su código de acceso:";

/* XTIT: Text of PasscodeInputController when user wants to enable touch ID. */
"passcodeInputControllerEnableTouchIDText" = "Para activar Touch ID, introduzca su código de acceso:";

/* XTIT: Title of Alert window when asking for confirmation about cancelling the onboarding flow. */
"rejectAlertControllerTitleText" = "Sin consentimiento, no podrá continuar con el proceso de incorporación.";

/* XBUT: Text of No button of reject. */
"rejectNoButtonText" = "Dar consentimiento";

/* XBUT: Text of Yes button of reject. */
"rejectYesButtonText" = "Salir del proceso";

/* XTIT: Info Screen text before presenting the SAML Authentication view. */
"samlAuthenticationStepInfoScreenText" = "Se están validando las credenciales SAML.";

/* XTIT: Info Screen text before presenting the SAP Mobile Services UserIdentity Discovery Authentication view. */
"sapcpmsUserIdentityDiscoveryAuthenticationStepInfoScreenText" = "Se están validando las credenciales UserIdentity Discovery de SAP Mobile Services.";

/* XTIT: Info Screen text before presenting the SLS Authentication view. */
"slsAuthenticationStepInfoScreenText" = "Se están validando las credenciales SLS.";

/* XMSG: Message for the user to fill all the fields for authentication. */
"slsFillAllOfTheFieldsErrorMessage" = "Rellene todos los campos";

/* XBUT: Text of Yes button. */
"yesButtonText" = "Sí";

/* XBUT: Text of Continue button. */
"continueButtonText" = "Continuar";

/* XBUT: Text of Cancel button. */
"cancelButtonText" = "Cancelar";

/* XTIT: Title of Usage Collection Consent onboarding flow. */
"UsageCollectionConsentTitle" = "Recopilación del uso";

/* XTIT: Body of Usage Collection Consent onboarding flow. */
"UsageCollectionConsentBody" = "El administrador de la aplicación solicita que permita la recopilación de datos del uso que haga de la aplicación para realizar análisis de cara a la mejora de la aplicación. La recopilación de datos incluye la información relacionada con su uso de la aplicación, como el tiempo de uso de la aplicación o cómo se desplaza por ella.";

/* XTIT: Usage Collection Consent opt-out alert message. */
"usageOptOutAlertMessage" = "¿Desea no participar?";

/* XTIT: Usage Collection Consent opt-in alert message. */
"usageOptInAlertMessage" = "¿Desea participar?";

/* XTIT: Title of Crash Report Collection Consent onboarding flow. */
"CrashReportCollectionConsentTitle" = "Recopilación de informes de incidentes";

/* XTIT: Body of Crash Report Collection Consent onboarding flow. */
"CrashReportCollectionConsentBody" = "El administrador de la aplicación solicita que permita la recopilación de datos para diagnosticar y analizar incidentes de la aplicación para mejorar su estabilidad. Los datos recopilados están limitados a los informes de incidentes y la información del dispositivo.";

