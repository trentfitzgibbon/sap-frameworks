// swift-tools-version:5.5

import PackageDescription

let package = Package(
    name: "SAPFrameworks",
    products: [
        .library(name: "SAPCommon", targets: ["SAPCommon"]),
        .library(name: "SAPFoundation", targets: ["SAPFoundation"]),
        .library(name: "SAPFiori", targets: ["SAPFiori"]),
        .library(name: "SAPOData", targets: ["SAPOData"]),
        .library(name: "SAPOfflineOData", targets: ["SAPOfflineOData"]),
        .library(name: "SAPFioriFlows", targets: ["SAPFioriFlows"]),
    ],
    dependencies: [
    ],
    targets: [
        .binaryTarget(name: "SAPCommon", path: "Frameworks/SAPCommon.xcframework"),
        .binaryTarget(name: "SAPFoundation", path: "Frameworks/SAPFoundation.xcframework"),
        .binaryTarget(name: "SAPFiori", path: "Frameworks/SAPFiori.xcframework"),
        .binaryTarget(name: "SAPOData", path: "Frameworks/SAPOData.xcframework"),
        .binaryTarget(name: "SAPOfflineOData", path: "Frameworks/SAPOfflineOData.xcframework"),
        .binaryTarget(name: "SAPFioriFlows", path: "Frameworks/SAPFioriFlows.xcframework"),
    ]
)
