# SAP Frameworks

## Purpose

This is a wrapper around all SAP frameworks, this wrapper is only to allow it to be built with SPM. The following are available through this package.

- SAPCommon
- SAPFoundation
- SAPFiori
- SAPOData
- SAPOfflineOData
- SAPFioriFlows

## Usage

To add this package on SPM add this into dependencies
``` swift
.package(
    name: "SAPFrameworks",
    url: "https://bitbucket.org/trentfitzgibbon/sap-frameworks.git",
    .exact("6.1.1")
)
```

This fetch the package you would like
``` swift
.product(name: "SAPFiori", package: "SAPFrameworks")
OR
.product(name: "SAPFioriFlows", package: "SAPFrameworks")
```

## Warning

Please use the following table to figure out which version of the package to use

| Package Version | Xcode Versions |
| -------- | -------- |
| 6.1.7 | 13.0+ |
| 6.1.1 | 12.1 - 12.5 |
| 0.2.0 | 11.3 - 12.5 |
